export default [
    {
        title: "Inflation Unisex Slippers",
        img:"https://images-na.ssl-images-amazon.com/images/I/61rOEJqTv4L._SY695._SX._UX._SY._UY_.jpg",
        price:13.50,
        prime:false,
        rating:4.1
    },
    {
        title:"Echo (2nd Generation)",
        img:"https://images-na.ssl-images-amazon.com/images/I/71YMatYkDPL._SX679_.jpg",
        price:79.99,
        prime:true,
        rating: 4.4

    },
    {
        title:"BRIGADA Minimalist Men's",
        img:"https://images-na.ssl-images-amazon.com/images/I/61EYZvYeC2L._AC_UY1000_.jpg",
        price:27.99,
        prime:true,
        rating: 4.5

    },
    {
        title:"TECKIN Smart Plug Mini",
        img:"https://images-na.ssl-images-amazon.com/images/I/51zSlwYrAQL._SX679_.jpg",
        price:23.18,
        prime:false,
        rating:4.5

    },
    {
        title:"ROCONTRIP Dry Bag Sack ",
        img:"https://images-na.ssl-images-amazon.com/images/I/512pVOV17KL._SX569_.jpg",
        price:30.59,
        prime:true,
        rating:4.6

    }
    ,
    {
        title:"AILLOSA Water Shoes",
        img:"https://images-na.ssl-images-amazon.com/images/I/71Y%2BnKuFUWL._UY695_.jpg",
        price:19.99,
        prime:true,
        rating:4

    }
    
]
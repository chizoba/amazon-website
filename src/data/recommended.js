export default [
    {
        title: "BROKIG Mens Jogging Pants",
        img:"https://images-na.ssl-images-amazon.com/images/I/618qGNLCTtL._UX679_.jpg",
        price:29.99,
        prime:true,
        rating:5
    },
    {
        title:"My Passport 1TB",
        img:"https://images-na.ssl-images-amazon.com/images/I/81dbJ1GJ6cL._SX679_.jpg",
        price:64.99,
        prime:true,
        rating: 4.6

    },
    {
        title:"QANSI Mens Sneakers",
        img:"https://images-na.ssl-images-amazon.com/images/I/71AQGw67QuL._UY695_.jpg",
        price:39.99,
        prime:false,
        rating: 4.3

    },
    {
        title:"Zinus Ultima Comfort Memory Foam",
        img:"https://images-na.ssl-images-amazon.com/images/I/81cSnpyMLhL._SX522_.jpg",
        price:212.14,
        prime:false,
        rating:4.2

    },
    {
        title:"Everybody Writes",
        img:"https://images-na.ssl-images-amazon.com/images/I/41ZDtvtLunL.jpg",
        price:19.99,
        kindle: true,
        prime:true,
        rating:4.3

    }
    
]
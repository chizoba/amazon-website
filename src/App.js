import React from 'react';
import logo from './logo.svg';
import './App.css';
import Navbar from './components/Navbar'
import Product from './components/Product'
import 'antd/dist/antd.css';
import {Icon} from 'antd'
import deals from './data/deals';
import recommendations from './data/recommended';

const dealsList = deals.map((deal,index)=>{
  return(
  <Product 
      title={deal.title}
      src={deal.img}
      rating={deal.rating}
      price={deal.price}
      prime={deal.prime}
      >
      

      </Product>)
})
const recommendedList = recommendations.map((deal,index)=>{
  return(
  <Product 
      title={deal.title}
      src={deal.img}
      rating={deal.rating}
      price={deal.price}
      prime={deal.prime}
      >
      </Product>)
})
function App() {
  return (
    <div className='container'>
     <div>
        <Navbar/>
         
        <div className="main">
       
          <div className="amazon-products">
<img src="https://images-na.ssl-images-amazon.com/images/G/15/kindle/merch/2019/XPL/FTVFL/CXL-994_SMP_FL_CA_GW_GW-Hero-IM2-3000x1200_EN._CB438105099_.jpg"/>
          </div>
        
          <div className='current-order'>
          <span><Icon type="bell" /> Amadi, your <strong>Amazon Echo</strong> is on its way</span>
          <a style={{display:'block', marginLeft:18}}>Track your order <Icon type="arrow-right" /></a>
        </div>

          <div className="section-wrapper">
              <p className='title'>Today's Deals</p>
              <div className='product-list'>
              {dealsList}
              </div>
              
          </div>
          <div className="section-wrapper">
              <p className='title'>Recommended for You</p>
              <div className='product-list'>
              {recommendedList}
              </div>
          </div>

        </div>
     </div>
    </div>
  );
}

export default App;

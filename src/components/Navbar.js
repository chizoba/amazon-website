import '../styles/navbar.css'
import React from 'react'
import {Input, Icon} from 'antd'
const {Search} = Input;

export default function Navbar(props){
    return (
        <div>
            <div className="top-nav">
            <div>
                <img src="https://upload.wikimedia.org/wikipedia/commons/a/a9/Amazon_logo.svg"
                style={{maxWidth:100}}
                />
                </div>
                <div className='search-wrapper'>
                <Search 
                
            placeholder="Search Amazon"
            style={{width:400,maxWidth:500, margin:"auto auto"}}/>
                {/* <span>
                 <Icon type="audio" />
                </span>
                <span>
                 <Icon type="camera" />
                </span> */}
                 
                </div>
                
                <div className='nav-buttons'>
                <span className="nav-button">
                <Icon type="appstore" />
                    <p>Menu</p>
                </span>
                <span className="nav-button">
                <Icon type="history" />
                    <p>Orders</p>
                </span>
                <span className="nav-button">
                <Icon type="bell" />
                <p>Notifications</p>
                </span>
                <span className="nav-button">
                <Icon type="shopping-cart" />
                <p>Cart</p>
                </span>
                <span className="nav-button">
                <Icon type="user" />
                <p>Hi, Amadi <Icon type="caret-down" /></p>
                </span>
                </div>
                
            </div>
            
            {/* <Search 
            placeholder="Search Amazon"
            style={{maxWidth:500, margin:"auto auto"}}/>
           <p>SHOP BY CATEGORIES</p> */}
           
        </div>
    )
}
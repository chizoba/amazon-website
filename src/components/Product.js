import React from 'react'
import '../styles/product.css'
import {Input, Icon} from 'antd'
const {Search} = Input;

export default function Product(props){
    return (
        <div className='product'>
            {props.prime && <img className='amazon-prime' src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e3/Amazon_Prime_Logo.svg/1200px-Amazon_Prime_Logo.svg.png"/>}
            <img src={props.src}/>
            <p className='product-title'>{props.title}</p>
            <p> from $ {props.price} CAD</p>
            <div className='product-buttons' >
            {/* <span className='product-button'>
            <Icon type="book" />
            </span >  */}
            <span className='product-button '>
             <Icon type="star" /> <span className='ratings'>{props.rating}</span>
          
            </span >
             <span className='product-button'>
             <Icon type="arrows-alt" />
            </span > 
            <span className='product-button'>
            <Icon type="shopping-cart" />
            </span>
            
            </div>
            {/* {props.children} */}
        </div>
    )
}
